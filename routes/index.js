const express = require('express');
const router = express.Router();

router.get('/status', (req, res) => {
  res.status(200).send('OK');
});

router.get('/city', (req, res) => {
  const cities = ['Paris', 'Bordeaux', 'Lyon', 'Strasbourg', 'Toulouse', 'Marseille'];
  res.status(200).json(cities);
});

router.use('*', (req, res) => {
  res.status(404).send('Not Found');
});

module.exports = router;
